# imports are libraries used in a program
# multiprocessing: needed to create a multiprocessing Pool,
# which can run things in parallel
import multiprocessing
# subprocess is used to call external programs (in the case RNAup)
import subprocess
# tqdm a handy tool to show progress
import tqdm
# argument parser to read in the input and output folder locations
# from the parameters
import argparse
# A representation of folders and files and an interface to easily
# manipulate them
from pathlib import Path


def processor(inout):
    """ Process an input file. Instead of using input, output and output_folder,
    We have to pass in a single parameter, as this is how we can communcate through
    multiprocessing.
    """
    # We need to open both the input and output files to be
    # able to read / write from / to them
    input_file = open(inout[0])
    output_file = open(inout[1], "w")
    # The folder is a simple string
    output_folder = inout[2]
    # This will call something on the operating system.
    # we have to separate each parameters as a list element.
    # stdin represents < and stdout represents >
    # cwd is the working folder, where python will run this command from.
    #   This is important as RNAup generates two output and one of them depends
    #   on where it was running from
    # communicate means run this command and wait until it finishes and gets back
    #   with a result
    subprocess.Popen(
        ["RNAup", "-b", "-d2", "--noLP", "-c", "'S'"], stdin=input_file, stdout=output_file, cwd=output_folder
    ).communicate()
    # After we're done with the work, we need to close the files
    input_file.close()
    output_file.close()


def walker(input_root: Path, output_root: Path):
    """ Walk through all the files which are under the directory input_root
    and ends with .fa
    """
    for file in input_root.glob('**/*.fa'):
        # file: represents a valid file (with .fa extension)
        # relative_to(input_root):
        #   Let's say, we have /input/All_5s_rrfF_input/TTATTTTCplasmid2.fa as file.
        #   input is /input
        #   we want to get All_5s_rrfF_input/TTATTTTCplasmid2.fa, because we
        #   want to maintain the same folder structure in the output directory
        # with_suffix('.out') 
        #   will change All_5s_rrfF_input/TTATTTTCplasmid2.fa to
        #   All_5s_rrfF_input/TTATTTTCplasmid2.out
        output_relpath = file.relative_to(input_root).with_suffix('.out')
        # This will complete our output filename: it will put together the
        # output folder with the final filename. For instance, if our output
        # folder is: /Users/david/rnaup_test/output, then the full path will be:
        # /Users/david/rnaup_test/output/All_5s_rrfF_input/TTATTTTCplasmid2.out
        output_file = output_root.joinpath(output_relpath)
        # This simply returns the folders where this new file will reside in:
        # /Users/david/rnaup_test/output/All_5s_rrfF_input
        output_folder = output_file.parent
        # This will ensure that the target folder exists (exist_ok=True means
        # it won't bother if the folder already exists)
        output_folder.mkdir(exist_ok=True)
        # This will return with these values every round
        # It is a list, where all the paths are absolute and are in text format,
        # instead of pythons Path object
        yield [str(file.absolute()), str(output_file.absolute()), str(output_folder.absolute())]


if __name__ == "__main__":
    # Read parameters when calling this script. python rnaup.py /input /output
    parser = argparse.ArgumentParser()
    parser.add_argument('input_root', type=Path)  # This is the first parameter
    parser.add_argument('output_root', type=Path)  # This is the second parameter
    # This is when the program attempts to read out the parameters which were
    # defined as arguments
    args = parser.parse_args()
    input_root = args.input_root
    output_root = args.output_root
   
    # Initialize a Pool that can utilize all the cores simulationously
    pool = multiprocessing.Pool()
    # tqdm: monitor how many files we processed and what is the speed
    # pool.imap_unordered: it tells that this Pool can execute things in
    # an arbitrary order, so the file order doesn't need to be kept
    # processor: this is the function that will be called for every file
    # walker: this is the function that walks through the files
    for _ in tqdm.tqdm(pool.imap_unordered(processor, walker(input_root, output_root))):
        pass
